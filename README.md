# 仿写网易云音乐实现音乐播放

## 技术栈
-  **vue+vuex+vue-router** 
- **vue-lazyload(图片懒加载)** 
-  **axios(数据请求)** 

## 软件架构

1.接口数据使用的是网易云官方提供的接口数据 只能本地使用


接口文件 **NeteaseCloudWebApp** 

接口文件不懂的可以直接看 里面的说明文件  或者直接去GitHub直接下载   https://github.com/Binaryify/NeteaseCloudMusicApi

下载之后直接
```
npm install
```


运行直接 


```
node app.js
```


2.音乐播放文件  **musicApp** 

安装依赖


```
npm install
```
运行


```
npm run dev

```

### 页面展示




![首页](https://images.gitee.com/uploads/images/2020/0320/143041_bb82f4da_4760192.png "微信图片_20200320142116.png")



### 歌单详情
![歌单详情](https://images.gitee.com/uploads/images/2020/0320/143456_5a17fe82_4760192.png "微信图片_20200320142205.png")

### 热歌榜页面

![歌单页面](https://images.gitee.com/uploads/images/2020/0320/143103_77149bdd_4760192.png "微信图片_20200320142214.png")



### 搜索页面


![搜索页面](https://images.gitee.com/uploads/images/2020/0320/143314_e2e68661_4760192.png "微信图片_20200320142209.png")



### 播放页面
![播放页面](https://images.gitee.com/uploads/images/2020/0320/143359_f4c7789b_4760192.png "微信图片_20200320142201.png")





需要改进的有很多，请大家可以多提提意见。后续我会不断改进，如果觉得还可以，请star，你们的star是我前进的动力。




